#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a target hash, crack it. Return the matching
// password.
char * crackHash(char *target, char *dictionary)
{
    // Open the dictionary file
    FILE *fp = fopen(dictionary, "r");
    char *entry = (char *)calloc(PASS_LEN, sizeof(char));

    // Loop through the dictionary file, one line
    // at a time.
    while (fgets(entry, PASS_LEN, fp))
    {
        entry[strlen(entry) - 1] = '\0';

        // Hash each password. Compare to the target hash.
        // If they match, return the corresponding password.
        char *hash = md5(entry, strlen(entry));
        if (strcmp(hash, target) == 0)
        {
            fclose(fp);
            return entry;
        }
    }

    // free up memory?
    free(entry);
    fclose(fp);
    return NULL;
}


int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Open the hash file for reading.
    FILE *fp = fopen(argv[1], "r");


    // For each hash, crack it by passing it to crackHash
    char hash[HASH_LEN];
    char *pass;
    while (fgets(hash, HASH_LEN, fp))
    {
        pass = crackHash(hash, argv[2]);

        // Display the hash along with the cracked password:
        // 5d41402abc4b2a76b9719d911017c592 hello
        if (pass)
            printf("%s %s\n", hash, pass);
    }


    // Close the hash file
    fclose(fp);

    // Free up any malloc'd memory?
    free(pass);
}
